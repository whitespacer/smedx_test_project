#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
//#include <cuda_runtime.h>

#define MAX_STRING 100
#define EXP_TABLE_SIZE 1000
#define MAX_EXP 6
#define MAX_CODE_LENGTH 40
#define MAX_SENTENCE_LENGTH 40
#define TRAIN_PAIRS_BATCH 400000
#define TRAIN_PAIRS_PER_THREAD 20 //TODO: check that TRAIN_PAIRS_BATCH % PAIRS_PER_THREAD == 0

typedef float real;                    // Precision of float numbers