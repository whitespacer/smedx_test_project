#include "main.h"

struct kernel_args {
  int* __restrict__ train_pairs;
  int* __restrict__ negative_samples;
  real alpha;
  long layer1_size;
  int vocab_size;
  volatile real *syn0;
  volatile real *syn1neg;
  real* __restrict__ expTable;
};

__inline__ __device__
real warp_reduce_sum(real val) {
  int offset;
  for (offset = warpSize/2; offset > 0; offset /= 2) 
    val += __shfl_down(val, offset);
  return val;
}

__inline__ __device__
real block_reduce_sum(real val) {  
  static __shared__ int shared[32]; // Shared mem for 32 partial sums
  int lane = threadIdx.x % warpSize;
  int wid = threadIdx.x / warpSize;
  val = warp_reduce_sum(val);     // Each warp performs partial reduction
  if (lane==0) shared[wid]=val; // Write reduced value to shared memory
  __syncthreads();              // Wait for all partial reductions
  //read from shared memory only if that warp existed
  val = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;
  if (wid==0) val = warp_reduce_sum(val); //Final reduce within first warp
  return val;
}

//int ctx_word, // aka last_word
//int central_word, // aka word

template <int negative_samples_count>
__global__ void  skip_gram(struct kernel_args args) {
  __shared__ real g;
  int tid = threadIdx.x;
  int from = blockIdx.y * 2 * TRAIN_PAIRS_PER_THREAD;

  for (int d = from; d < from + 2 * TRAIN_PAIRS_PER_THREAD; d += 2) {
    int ctx_word = args.train_pairs[d];

    int target = args.train_pairs[d + 1];
    int ctx_position = ctx_word * args.layer1_size;
    int target_position = target * args.layer1_size;
    real syn0 = args.syn0[tid + ctx_position];
    real syn1neg = args.syn1neg[tid + target_position];
    real f = block_reduce_sum(syn0 * syn1neg);
    if (tid == 0) {
       if (f > MAX_EXP) g = 0;
       else if (f < -MAX_EXP) g = args.alpha;
       else g = (1 - args.expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * args.alpha;
    }
    __syncthreads();

    args.syn1neg[tid + target_position] += g * syn0;
    syn0 += syn1neg * g;

    for (int negative_sample = 0; negative_sample < negative_samples_count; ++negative_sample)
    {
       target = args.negative_samples[negative_sample];
       target_position = target * args.layer1_size;
       syn1neg = args.syn1neg[tid + target_position];
       f = block_reduce_sum(syn0 * syn1neg);

       if (tid == 0) {
          if (f > MAX_EXP) g = -args.alpha;
          else if (f < -MAX_EXP) g = 0;
          else g = -args.expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))] * args.alpha;
       }
       __syncthreads();

       //TODO: new problem here - concurent access to args.syn1neg[tid + target_position]
       //SOLUTION: write to separate array, add in other kernel
       args.syn1neg[tid + target_position] += g * syn0;
       syn0 += g * syn1neg;
    }

    args.syn0[tid + ctx_position] = syn0;
  }
}